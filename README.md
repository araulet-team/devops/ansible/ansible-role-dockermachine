# Ansible Role Docker Machine

Role for installing docker-machine.

## Requirements
This role requires Ansible 1.4 or higher, and platform requirements are listed in the metadata file.

## Installation

add this in **requirements.yml**

```shell
- src: https://gitlab.com/araulet-team/devops/ansible/ansible-role-dockermachine
  name: araulet.dockermachine
```

## Role Variables
The variables that can be passed to this role and a brief description about them are as follows:

```yaml
roles:
  - role: araulet.dockermachine
    become: true
    vars:
      docker_install_machine: true
      docker_machine_version: 0.14.0
```
## Dependencies

None

## Licence
MIT

## Author Information
Arnaud Raulet